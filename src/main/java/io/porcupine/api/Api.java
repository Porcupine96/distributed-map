package io.porcupine.api;

import io.porcupine.map.SimpleStringMap;

import static spark.Spark.*;

public class Api {

    private final SimpleStringMap map;

    public Api(SimpleStringMap map) {
        this.map = map;
    }

    public void setupEndpoints() {
        get("/map/:key", (req, res) -> neverNull(map.get(req.params(":key"))));
        put("/map/:key/:value", (req, res) -> neverNull(map.put(req.params(":key"), req.params(":value"))));
        delete("/map/:key", (req, res) -> neverNull(map.remove(req.params(":key"))));
        get("/map/contains/:key", (req, res) -> map.containsKey(req.params(":key")));
    }

    private String neverNull(String result) {
        if (result == null) return "";
        else return result;
    }

}
