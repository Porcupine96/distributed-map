package io.porcupine;

import io.porcupine.map.Cluster;
import io.porcupine.map.DistributedMap;
import io.porcupine.map.SimpleStringMap;
import io.porcupine.message.MessageAdapter;
import io.porcupine.message.ProtoMessageAdapter;
import org.jgroups.protocols.UDP;

import java.net.InetAddress;

public class LocalDemo {

    public static void main(String[] args) throws Exception {
        System.setProperty("java.net.preferIPv4Stack", "true");



        Cluster one = new Cluster();
        Cluster two = new Cluster();
        MessageAdapter messageAdapter = new ProtoMessageAdapter();

        SimpleStringMap first = new DistributedMap(one, messageAdapter);
        first.put("hello", "world");

        Thread.sleep(3000);

        SimpleStringMap second = new DistributedMap(two, messageAdapter);

        Thread.sleep(3000);

        System.out.println(second.get("one"));
    }

}
