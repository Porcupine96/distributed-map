package io.porcupine;

import io.porcupine.api.Api;
import io.porcupine.map.Cluster;
import io.porcupine.map.DistributedMap;
import io.porcupine.map.SimpleStringMap;
import io.porcupine.message.MessageAdapter;
import io.porcupine.message.ProtoMessageAdapter;

import java.net.UnknownHostException;
import java.util.Optional;

import static spark.Spark.port;

public class Main {

    public static void main(String[] args) throws Exception {
        configure();

        Cluster cluster = new Cluster();
        MessageAdapter messageAdapter = new ProtoMessageAdapter();
        SimpleStringMap map = new DistributedMap(cluster, messageAdapter);

        Api api = new Api(map);
        api.setupEndpoints();
    }

    private static void configure() {
        System.setProperty("java.net.preferIPv4Stack", "true");
        Optional<String> apiPort = Optional.ofNullable(System.getenv("API_PORT"));
        port(Integer.valueOf(apiPort.orElse("8080")));
    }

}
