package io.porcupine.map;

import org.jgroups.Address;
import org.jgroups.JChannel;
import org.jgroups.MergeView;
import org.jgroups.View;

import java.util.List;

class ViewHandler extends Thread {
    private final JChannel channel;
    private final MergeView view;

    ViewHandler(Cluster cluster, MergeView view) {
        this.channel = cluster.getChannel();
        this.view = view;
    }

    public void run() {
        System.out.println("RUNNING_VIEW_HANDLER");

        List<View> subgroups = view.getSubgroups();
        View tmpView = subgroups.get(0);

        Address localAddress = channel.getAddress();

        if (!tmpView.getMembers().contains(localAddress)) {
            System.out.println("Not member of the new primary partition (" + tmpView + "), will re-acquire the state");
            try {
                channel.getState(null, 10000);
                System.out.println("VIEW_HANDLER_GOT_NEW_STATE");
            } catch (Exception ex) {
                System.out.println("Exception while getting the state " + ex.getMessage());
            }
        } else {
            System.out.println("Not member of the new primary partition (" + tmpView + "), will do nothing");
        }
    }

}