package io.porcupine.map;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.protocols.*;
import org.jgroups.protocols.pbcast.*;
import org.jgroups.stack.ProtocolStack;

import java.net.InetAddress;

public class Cluster {

    private final JChannel channel;

    public Cluster() throws Exception {
        JChannel channel = new JChannel();
        ProtocolStack stack = new ProtocolStack();
        stack.addProtocol(new UDP().setValue("mcast_group_addr", InetAddress.getByName("230.0.0.11")))
                .addProtocol(new PING())
                .addProtocol(new MERGE3())
                .addProtocol(new FD_SOCK())
                .addProtocol(new FD_ALL().setValue("timeout", 12000).setValue("interval", 3000))
                .addProtocol(new VERIFY_SUSPECT())
                .addProtocol(new BARRIER())
                .addProtocol(new NAKACK2())
                .addProtocol(new UNICAST3())
                .addProtocol(new STABLE())
                .addProtocol(new SEQUENCER())
                .addProtocol(new FLUSH())
                .addProtocol(new GMS())
                .addProtocol(new UFC())
                .addProtocol(new MFC())
                .addProtocol(new FRAG2())
                .addProtocol(new STATE());

        channel.setProtocolStack(stack);

        stack.init();

        this.channel = channel;
    }

    public void connect(String clusterName) throws Exception {
        channel.connect(clusterName);
    }

    public void getState() throws Exception {
        channel.getState(null, 10000);
    }

    public void sendToAll(byte[] message) {
        Message jMessage = new Message(null, null, message);
        try {
            channel.send(jMessage);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void setReceiver(ReceiverAdapter receiverAdapter) {
        this.channel.setReceiver(receiverAdapter);
    }

    public JChannel getChannel() {
        return channel;
    }

}
