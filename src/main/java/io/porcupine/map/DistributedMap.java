package io.porcupine.map;

import io.porcupine.message.DomainMessage;
import io.porcupine.message.MessageAdapter;
import io.porcupine.message.PutMessage;
import io.porcupine.message.RemoveMessage;
import org.jgroups.MergeView;
import org.jgroups.Message;
import org.jgroups.ReceiverAdapter;
import org.jgroups.View;
import org.jgroups.util.Util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DistributedMap extends ReceiverAdapter implements SimpleStringMap {

    private final Map<String, String> map = new ConcurrentHashMap<>();
    private final Cluster cluster;
    private final MessageAdapter messageAdapter;

    public DistributedMap(Cluster cluster, MessageAdapter messageAdapter) throws Exception {
        this.cluster = cluster;
        this.messageAdapter = messageAdapter;
        this.cluster.setReceiver(this);
        this.cluster.connect("MyChannel");
        this.cluster.getState();
    }

    @Override
    public void receive(Message msg) {
        DomainMessage message = messageAdapter.toDomain(msg.getRawBuffer());

        switch (message.getType()) {
            case PUT:
                PutMessage putMessage = (PutMessage) message;
                map.put(putMessage.key, putMessage.value);
                break;
            case REMOVE:
                RemoveMessage removeMessage = (RemoveMessage) message;
                map.remove(removeMessage.key);
                break;
        }
    }

    @Override
    public void getState(OutputStream output) throws Exception {
        System.out.println("GET_STATE");
        Util.objectToStream(map, new DataOutputStream(output));
    }

    @Override
    public void setState(InputStream input) throws Exception {
        System.out.println("SET_STATE");
        Map<String, String> state = (Map<String, String>) Util.objectFromStream(new DataInputStream(input));
        state.values().forEach(System.out::println);
        map.clear();
        map.putAll(state);
    }

    @Override
    public void viewAccepted(View view) {
        System.out.println("VIEW_ACCEPTED");
        handleView(cluster, view);
    }

    private static void handleView(Cluster cluster, View newView) {
        if (newView instanceof MergeView) {
            System.out.println("MERGE_VIEW");
            ViewHandler handler = new ViewHandler(cluster, (MergeView) newView);
            handler.start();
        }
    }

    @Override
    public boolean containsKey(String key) {
        System.out.println("CONTAINS [" + key + "]");
        return map.containsKey(key);
    }

    @Override
    public String get(String key) {
        System.out.println("GET [" + key + "]");
        return map.get(key);
    }

    @Override
    public String put(String key, String value) {
        System.out.println("PUT [" + key + ", " + value + "]");
        byte[] rawMessage = messageAdapter.toRaw(new PutMessage(key, value));
        cluster.sendToAll(rawMessage);
        return map.put(key, value);
    }

    @Override
    public String remove(String key) {
        System.out.println("REMOVE [" + key + "]");
        byte[] rawMessage = messageAdapter.toRaw(new RemoveMessage(key));
        cluster.sendToAll(rawMessage);
        return map.remove(key);
    }

}
