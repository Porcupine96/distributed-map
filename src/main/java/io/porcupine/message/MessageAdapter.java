package io.porcupine.message;

public interface MessageAdapter {

    byte[] toRaw(DomainMessage message);

    DomainMessage toDomain(byte[] rawMessage);

}
