package io.porcupine.message;

public class RemoveMessage implements DomainMessage {
    public final String key;

    public RemoveMessage(String key) {
        this.key = key;
    }

    @Override
    public MessageType getType() {
        return MessageType.REMOVE;
    }

    @Override
    public String toString() {
        return "RemoveMessage{" +
                "key='" + key + '\'' +
                '}';
    }
}
