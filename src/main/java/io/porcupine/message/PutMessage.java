package io.porcupine.message;

public class PutMessage implements DomainMessage {
    public final String key;
    public final String value;

    public PutMessage(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public MessageType getType() {
        return MessageType.PUT;
    }

    @Override
    public String toString() {
        return "PutMessage{" +
                "key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
