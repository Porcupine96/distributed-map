package io.porcupine.message;

public interface DomainMessage {

    MessageType getType();

}
