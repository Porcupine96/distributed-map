package io.porcupine.message;

public enum MessageType {
    PUT, REMOVE
}
