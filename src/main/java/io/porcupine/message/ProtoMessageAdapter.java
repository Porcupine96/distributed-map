package io.porcupine.message;


import com.google.protobuf.InvalidProtocolBufferException;
import io.porcupine.proto.Message;

public class ProtoMessageAdapter implements MessageAdapter {

    @Override
    public byte[] toRaw(DomainMessage message) {
        switch (message.getType()) {
            case PUT:
                PutMessage put = (PutMessage) message;
                return rawPut(put.key, put.value);

            case REMOVE:
                RemoveMessage remove = (RemoveMessage) message;
                return rawRemove(remove.key);
        }
        throw new IllegalArgumentException("Unsupported message type " + message.getType());
    }

    private byte[] rawPut(String key, String value) {
        return Message.Update
                .newBuilder()
                .setKey(key)
                .setValue(value)
                .setType(Message.Update.Type.PUT)
                .build()
                .toByteArray();
    }

    private byte[] rawRemove(String key) {
        return Message.Update
                .newBuilder()
                .setKey(key)
                .setType(Message.Update.Type.REMOVE)
                .build()
                .toByteArray();
    }

    @Override
    public DomainMessage toDomain(byte[] rawMessage) {
        try {
            Message.Update message = Message.Update.parseFrom(rawMessage);
            switch (message.getType()) {
                case PUT:
                    return new PutMessage(message.getKey(), message.getValue());
                case REMOVE:
                    return new RemoveMessage(message.getKey());
            }
            throw new IllegalArgumentException("Unsupported message type " + message.getType());
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException(e);
        }
    }

}
