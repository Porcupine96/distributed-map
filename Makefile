net:
	docker network create net

all: one two three

one:
	docker run -d -p 8080:8080 --name one --network=net io.porcpupine/distributed-hashmap:0.1

two:
	docker run -d -p 8081:8080 --name two --network=net io.porcpupine/distributed-hashmap:0.1

three:
	docker run -d -p 8082:8080  --name three --network=net io.porcpupine/distributed-hashmap:0.1

put:
	curl -X PUT localhost:$(port)/map/$(key)/$(value)

get:
	curl localhost:$(port)/map/$(key)

stop:
	docker stop one
	docker rm one
	docker stop two
	docker rm two
	docker stop three
	docker rm three

